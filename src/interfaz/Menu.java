/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import java.util.ArrayList;

/** Declaracion de la Clase Menu.
 *
 * @author Brank
 * @version 09/07/2019
 */
public abstract class Menu {
    //Declaracion de atributo por defecto.
    ArrayList<String> opciones;
    /** Creacion del constructor.
     * 
     * @param opciones Indica las distintas opciones del Menu. 
     */
    public Menu(ArrayList<String> opciones) {
        this.opciones = opciones;
    }
    /** Metodo cargarMenu() metodo que se sobrescribirá en las subclases.
     * 
     */
    public void cargarMenu() {
    }
    /** Metodo mostrarMenu() Enlista las opciones de los menus a mostrar.
     * 
     */
    public void mostrarMenu() {
        Util.limpiarPantalla();        
        System.out.println("================================================================================");
        System.out.println();       
        for (Integer i = 0; i < opciones.size(); i += 1) {
            System.out.println(i + 1 + ". " + opciones.get(i));
        }
        System.out.println();
        System.out.println("================================================================================");
    }    
}
